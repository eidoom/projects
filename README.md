# projects

[Live](https://eidoom.gitlab.io/projects)

## Developing

```bash
npm install
npm run dev
```

## Building

```bash
npm run build
```

## Style guide

Images should be 256x256 WebP at around 70% quality.

LoC calculated by
```bash
cloc --vcs=git --no-autogen --exclude-lang="Markdown,TOML,make,Dockerfile,m4,YAML,JSON,Bourne Again Shell,Bourne Shell,CSV,TeX,Text,Meson,SVG"
```
to exclude notes, auto-generated code and build scripts.

## Ideas

It would be fun to give this a backend that would automatically grep statistics from matching projects in a directory of git repositories.
It could regularly pull the repos and track:

* number of Git commits
* active days = days on which commits were made
* activity calendar (global and by project)
* dates of first and last commit
* LoC (by language and sum) by calling `cloc`
