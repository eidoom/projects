import adapter from "@sveltejs/adapter-static";
import * as child_process from "node:child_process";
import { vitePreprocess } from "@sveltejs/vite-plugin-svelte";

export default {
  kit: {
    adapter: adapter({ fallback: "404.html" }),
    paths: {
      base: process.argv.includes("dev") ? "" : process.env.BASE_PATH,
    },
    version: {
      name: child_process
        .execSync("git rev-parse --short HEAD")
        .toString()
        .trim(),
    },
  },
  preprocess: vitePreprocess(),
};
