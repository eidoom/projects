import data from "./items.yaml";

function unique_link(counts, host, dirs) {
  const dropHost = ["eidoom.gitlab.io"];
  if (dropHost.includes(host)) {
    return dirs[1];
  }

  const takeFirstDomain = ["gitlab.com", "github.com"];
  const out_host = takeFirstDomain.includes(host) ? host.split(".")[0] : host;

  const skipTopDir = [...takeFirstDomain];
  return (
    out_host +
    (counts[host] > 1 ? "/" + dirs[skipTopDir.includes(host) ? 2 : 1] : "")
  );
}

function process(list, itemType) {
  return list
    .map((item) => ({
      ...item,
      tags: (item.tags ?? []).sort(),
      links: (item.links ?? []).map((link) => {
        const url = new URL(link);
        return {
          href: url.href,
          host: url.host,
          dirs: url.pathname.split("/"),
        };
      }),
      desc: (item.desc ?? "").replace(
        /\[(.*)\]\((.*)\)/,
        '<a href="$2" class="text-sky-600 dark:text-sky-300">$1</a>',
      ),
      type: itemType,
    }))
    .map((item) => {
      // count how many times each host appears
      const counts = (item.links ?? [])
        .map((link) => link.host)
        .reduce((acc, cur) => ((acc[cur] = ++acc[cur] || 1), acc), {});
      return {
        ...item,
        links: (item.links ?? []).map((link) => ({
          href: link.href,
          host: unique_link(counts, link.host, link.dirs),
        })),
      };
    });
}

export function load() {
  return {
    items: [
      ...process(data.projects, "project"),
      ...process(data.minis, "mini"),
    ].filter((i) => !i.draft),
  };
}
